package starter.navigation;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.PageObjects;
import net.thucydides.core.annotations.DefaultUrl;

public class ConsulateHomePage {
    @DefaultUrl("https://citas.sre.gob.mx/")
    public class ConsulateHomePage extends PageObject {}
}
